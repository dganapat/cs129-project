# CS129 Project

If we use streamlit we can integrate our paper with our plots and the code for the plots so everything is all in one place and can be updated live. This repository will also be better organized so that we only have the plots and the scripts that we need. We can choose to show snippets of our code in the paper if we want, and also this will upload well as the SI for our paper when we submit it.

Repository Information:
    In Gitlab (code.stanford.edu), go the cs129 project, click on the blue clone button in the top right, and copy the "clone with SSH" link.
    In your terminal (look up how to access this for your operating system), navigate to the folder where you want to clone the repository into.
    The repository will clone as a folder itself, so you don't need to create an extra parent directory for it.
    In the terminal, type the command:
        git clone (the link you copied from gitlab, without the parentheses)
    You should see this as a new folder on your system


Virtual Environment Information:
    Make sure you have conda installed on your computer and up to date
    Following these instructions: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/
    In the terminal, type: conda create -n cs129 python=3.7 anaconda
        You can replace cs129 with whatever you want your virtual environment to be called
    To activate virtual environnment, in the terminal, type: conda activate cs129
        You should see (cs129) in front of your command line input now

    .py files: When in vscode, make sure you select the right interpreter by clicking on Python X.X.X in the bottom left corner, then selecting the (cs129) interpreter from the top menu. You can also navigate to this by pressing command+shift+p and typing interpreter -> select interpreter -> choose the one in the virtual environment. You might need to restart VSCode to get it to show up.

    .ipynb files: when in vscode, select the right kernel by clicking on "Python X: XXXX" in the top right corner, then selecting the Python 3 option that ends in something like "envs/cs129/bin/python" from the dropdown menu
    

Python 3.7.9 64-bit
Packages to install (with commands):
    rdkit: (while in virtual environment) conda install -c rdkit rdkit
    mordred: (while in virtual environment) conda install -c mordred-descriptor mordred 

All other required packages are installed as dependencies of the above packages. 

